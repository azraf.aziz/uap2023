package Contest;

class CAT8 extends TiketKonser {
    public CAT8(String nama, double harga) {
        super(nama, harga);//super ambil dari parent
    }

    @Override
    public double hitungHarga() {
        return getHarga_tiket();
    }
}