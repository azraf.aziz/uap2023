package Contest;

class PemesananTiket {
    private static TiketKonser[] tiketKonser;// menggunakan static agar bisa diakses di semua class
    static {//menggunakan satatic
        tiketKonser = new TiketKonser[]{
            new CAT8("CAT 8", 10),//membuat objecct baru sesuai dengan tipe cat8,cat1 dan dst harganya kita buat murah aja ygy
            new CAT1("CAT 1", 15),
            new FESTIVAL("FESTIVAL", 20),
            new VIP("VIP", 30),
            new VVIP("UNLIMITED EXPERIENCE", 40)
        };
    }

    public  static TiketKonser pilihTiket(int index) {
        return tiketKonser[index];
    }
}