/*
 * UAP PEMLAN 2023
 * DURASI: 120 MENIT
 * TEMPAT PENGERJAAN: DARING/LURING
 * =================================================================
 * Semangat mengerjakan UAP teman-teman
 * Jangan lupa berdoa untuk hasil yang terbaik
 */

package Contest;

import java.util.*;
import java.text.SimpleDateFormat;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Selamat datang di Pemesanan Tiket Coldplay!");
        boolean tiketbelumselesaidipesan = true;
        while(tiketbelumselesaidipesan){
            try {
                System.out.print("Masukkan nama pemesan : ");
                String namaPemesan = scanner.nextLine();
                System.out.println("Pilih jenis tiket : ");
                System.out.println("1. CAT 8");
                System.out.println("2. CAT 1");
                System.out.println("3. FESTIVAL");
                System.out.println("4. VIP");
                System.out.println("5. UNLIMITED EXPERIENCE");
                System.out.print("Masukkan pilihan : ");
                int angka;
                try {
                    angka = Integer.parseInt(scanner.nextLine());//pake ini biasanya kalo pake next int nanti pas ngulangi erorr ga jelas
                } catch (NumberFormatException e) {//mengeluarkan error ketika fomar input tidak sesuai permintaan
                    throw new InvalidInputException("Input Anda Salah, Masukkan pilihan tiket dengan angka yang benar!");
                }if (angka < 1 || angka > 5) {//mengeluarkan error angka yang dipilih kurang dari satu dan lebih dari 5
                    throw new InvalidInputException("Input Anda Salah, Pilahan Cuman nomor 1-5!");
                }
                TiketKonser tiket = PemesananTiket.pilihTiket(angka - 1);//dikurang satu karena array mulai dari 1
                System.out.println("----- Detail Pemesanan -----");
                System.out.println("Nama Pemesan: " + namaPemesan);//memanggil nama pemesan
                System.out.println("Kode Booking: " + generateKodeBooking());//memanggil methid get kode boking
                System.out.println("Tanggal Pesanan: " + getCurrentDate());//memanggil method format date untuk mendapat format ate terkini
                System.out.println("Tiket yang dipesan: " + tiket.getNama());//mendapatkan nama jenis tiket yang dipesan
                System.out.println("Total harga: " + tiket.hitungHarga() + " USD");
                System.out.println("----------------------------");
                System.out.println("Terimakasih telah memesan Tiket kami, Pesanan Anda Telah Selesai Diproses!");
                tiketbelumselesaidipesan = false;//ketika sudah selesai mesan tidak bisa mesen lagi ini tiket coldyplay suman bisa sekali yaa..
            } catch (InvalidInputException e) {
                System.out.println(e.getMessage()+"\nSilahkan Coba Lagi yang benar!");//menggeluarkan ini ketika error
            } 
        }
     
    }

    /*
     * Jangan ubah isi method dibawah ini, nama method boleh diubah
     * Method ini dipanggil untuk mendapatkan kode pesanan atau kode booking
     * Panggil method ini untuk kelengkapan mencetak output nota pesanan
     */
    public static String generateKodeBooking() {
        StringBuilder sb = new StringBuilder();
        String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        int length = 8;

        for (int i = 0; i < length; i++) {
            int index = (int) (Math.random() * characters.length());
            sb.append(characters.charAt(index));
        }

        return sb.toString();
    }

    /*
     * Jangan ubah isi method dibawah ini, nama method boleh diubah
     * Method ini dipanggil untuk mendapatkan waktu terkini
     * Panggil method ini untuk kelengkapan mencetak output nota pesanan
     */
    public static String getCurrentDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Date currentDate = new Date();
        return dateFormat.format(currentDate);
    }
}