package Contest;

abstract class TiketKonser implements HargaTiket {
    private String nama_tiket;
    private double harga_tiket;

    public double getHarga_tiket() {
        return harga_tiket;
    }

    public TiketKonser(String nama, double harga) {
        this.nama_tiket = nama;
        this.harga_tiket = harga;
    }

    public String getNama() {
        return nama_tiket;
    }

    public abstract double hitungHarga();
}