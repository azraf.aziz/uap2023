package Contest;

class CAT1 extends TiketKonser {
    public CAT1(String nama, double harga) {
        super(nama, harga);//super ambil dari parent
    }

    @Override
    public double hitungHarga() {
        return getHarga_tiket();
    }
}