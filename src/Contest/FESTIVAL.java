package Contest;

class FESTIVAL extends TiketKonser {
    public FESTIVAL(String nama, double harga) {
        super(nama, harga);//super ambil dari parent
    }

    @Override
    public double hitungHarga() {
        return getHarga_tiket();
    }
}